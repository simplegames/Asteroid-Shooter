# Asteroid Shooter
A simple game made for a friend's school project. The requirements were to use python and turtle. 

## Features
- Used Analytic geometry to calculate intersections between player and asteroids
- OOP design

## Used technologies
- [Turtle](https://docs.python.org/3/library/turtle.html) - Small python library for graphics
